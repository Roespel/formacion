@extends('adminlte::page')

@section('title', 'Formación')

@section('content_header')

    <h1>CURSOS</h1>
@stop

@section('content')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <p></p>
   
            <a href="cursos/create" class="btn btn-primary">CREAR</a>
    <table class="table table-dark table-striped mt-4">
            <thead>
                <tr> 
                    <th scope="col">id</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Activo</th>
                    <th scope="col">Acciones</th>
                </tr>   
            </thead>
            <tbody>
                @foreach ($cursos as $curso)
                
                <tr>    
                    <td>{{$curso->id}}</td>
                    <td>{{$curso->nombre}}</td>
                    <td>{{$curso->activo}}</td> 
                    <td>
                    <a href="" class="btn btn-info">Editar</a>
                    <button class="btn btn-danger">Borrar</button>
                    </td> 
                </tr>    
                    @endforeach
                   
            </tbody>

    </table>



@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    
@stop